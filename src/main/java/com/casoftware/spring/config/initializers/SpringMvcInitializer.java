package com.casoftware.spring.config.initializers;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.casoftware.security.OAuth2ServerConfiguration;
import com.casoftware.security.WebSecurityConfig;
import com.casoftware.spring.config.EmailConfig;


public class SpringMvcInitializer  extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {com.casoftware.spring.config.AppConfig.class, WebSecurityConfig.class, OAuth2ServerConfiguration.class, EmailConfig.class};
	}
	
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}
		
}